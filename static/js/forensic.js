class forensic extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
    }
    beforeParsed(content) {
        let panneau = document.createElement("aside");
        let liste = document.createElement("ul");
        panneau.style = "position : fixed;top:30px;left:30px;border:1px solid purple;padding:5ch;z-index:999999;background-color:rgba(255,255,255,.8";
        panneau.insertAdjacentHTML(`beforeend`,
            `<h4>Classes dans la source</h4>`
        );
        panneau.appendChild(liste);
        // let’s get all the classes affected to elements
        const sections = content.querySelectorAll('*');
        var classes = [];
        sections.forEach(element => {
            if (element.classList.value != '') { // if element has some classes
                if (element.classList.value.includes(" ")) { // more than one class?
                    let coupage = element.classList.value.split(" ");
                    coupage.forEach(untag => {
                        classes.push(untag);
                    });
                } else { // one class?
                    classes.push(element.classList.value);
                }
            }
        });
        classes.sort(); // class name sorting
        let uniqueClassStack = new Set(classes); // getting unique values
        uniqueClassStack.forEach(lesclaseses => {
            liste.insertAdjacentHTML(`beforeend`,
                `<li>${lesclaseses}</li>`
            )
        });
        liste.childNodes.forEach(item => { // highlighting concered elements
            item.addEventListener("click", (e) => {
                let selecteur = "." + item.textContent;
                let concernes = document.querySelectorAll(selecteur);
                concernes.forEach(lm => {
                    if (lm.style.backgroundColor === "pink") {
                        lm.style.backgroundColor = "transparent";
                        console.log("off");
                    } else {
                        lm.style.backgroundColor = "pink";
                        console.log("on");
                    }
                });
            });
        });
        document.querySelector("body").appendChild(panneau);
    }
}
Paged.registerHandlers(forensic);